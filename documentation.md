# Documentation

À destination des personnes amenées à faire évoluer le contenu de l'application [Francil'IN Carto](https://francilin-carto.netlify.app/).

## Contexte

Le contenu de l'application Francil'IN Carto est principalement composé de :

- Données sur les lieux d'inclusion numérique en Île-de-France.
- Textes internationalisés.

## Données sur les lieux

Ces données sont à l'heure actuelle maintenues dans une feuille de tableur intitulée `SOURCE - Fichier normalisé` sur le Drive Synology hébergé par Francil'IN. La structure de ce fichier tabulaire, c'est à dire ses colonnes, leur label et leur format, ne doit pas être changé afin de ne pas rompre la chaîne du traitement.

Avant d'être intégrées dans l'application, ces données sont enregistrées dans le fichier [`source_fichier_normalise.csv`](https://gitlab.com/francilin/francilin-data/-/blob/master/source/) au sein du dépôt [`francilin-data`](https://gitlab.com/francilin/francilin-data/).

### Marche à suivre pour Francil'IN

Pour mettre à jour les données dans l'application, veuillez suivre les étapes suivantes :

1. Apporter des modifications dans `SOURCE - Fichier normalisé` sur le Drive Synology en veillant à respecter les instructions ;
2. Télécharger le fichier au format Open Document (`.ods`) depuis l'interface du tableur ;
3. [Créer un nouveau ticket](https://gitlab.com/francilin/francilin-data/-/issues/new) en précisant la demande et en attachant le fichier.

Une personne habilitée (chez Jailbreak) pourra alors suivre [la procédure](https://gitlab.com/francilin/francilin-data-processing#mise-%C3%A0-jour-des-donn%C3%A9es) afin d'intégrer les modifications dans l'application.

## Textes internationalisés

Tous les textes de l'application qui ont fait l'objet d'une internationalisation sont contenus dans le fichier [`messages.csv`](https://gitlab.com/francilin/francilin-carto/-/blob/master/src/messages/messages.csv) au sein du dépôt [`francilin-carto`](https://gitlab.com/francilin/francilin-carto/).

**Attention :** Les intitulés et descriptions des services et thématiques, puisqu'ils sont traduits eux aussi, ne sont plus récupérés depuis la feuille `Referentiel Services` sur le Drive Synology. Nous n'utilisons plus du tout ce fichier, par conséquent si vous modifiez le référentiel (thématiques, services, ainsi que leurs intitulés et descriptions) vous devrez obligatoirement répercuter également les modifications dans le fichier `messages.csv`.

### Marche à suivre pour Francil'IN

Pour mettre à jour les textes internationalisés dans l'application, veuillez suivre les étapes suivantes :

1. Télécharger le fichier depuis [ce lien](https://gitlab.com/francilin/francilin-carto/-/raw/master/src/messages/messages.csv?inline=false). Attention : Il est essentiel que vous téléchargiez d'abord la dernière version du fichier avant d'y apporter des modifications ;
2. Ouvrir le fichier dans un tableur comme Excel ou Calc (sélectionnez le format UTF-8 pour éviter les erreurs d'encodage) ;
3. Modifier le contenu du texte dans le fichier (colonnes `fr`, `en`, `ar`, etc.) et en ne touchant surtout pas à la colonne `key` ;
4. Enregistrer le fichier en gardant exactement le même format (CSV séparé par des virgules, encodage UTF-8) ;
5. [Créer un nouveau ticket](https://gitlab.com/francilin/francilin-carto/-/issues/new) en précisant la demande et en attachant le fichier.

Une personne habilitée (chez Jailbreak) pourra alors suivre [la procédure](https://gitlab.com/francilin/francilin-carto#updating-i18n-messages) afin d'intégrer les modifications dans l'application.
