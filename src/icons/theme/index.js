import theme_1 from "./theme_1.svg"
import theme_2 from "./theme_2.svg"
import theme_3 from "./theme_3.svg"
import theme_4 from "./theme_4.svg"
import theme_5 from "./theme_5.svg"
import theme_6 from "./theme_6.svg"
import theme_7 from "./theme_7.svg"
import theme_8 from "./theme_8.svg"
import theme_9 from "./theme_9.svg"
import theme_10 from "./theme_10.svg"
import theme_default from "./theme_default.svg"

export default {
	theme_1,
	theme_2,
	theme_3,
	theme_4,
	theme_5,
	theme_6,
	theme_7,
	theme_8,
	theme_9,
	theme_10,
	theme_default,
}
