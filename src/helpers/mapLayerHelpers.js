const mainThemePropertyName = "thematique_principale"
let pixelRatio = Math.min(2, Math.floor(window.devicePixelRatio))

export const iconImageExpression = ["concat", "theme-", ["get", mainThemePropertyName]]

export const clusterLayerFilter = ["has", "point_count"]

// prettier-ignore
export const clusterLayerLayout = {
  "icon-allow-overlap": true,
	"text-allow-overlap": true,
  "icon-image": "marker-cluster",
  "icon-anchor": "bottom",
  "icon-offset": [0, 36],
  "icon-size": ["step", ["get", "point_count"],
    .16,
    10, .2,
    30, .25,
    50, .32,
    100, .4,
  ],
  "text-offset": ["step", ["get", "point_count"],
    ["literal", [0,-2.3]],
    10, ["literal", [0,-2.5]],
    30, ["literal", [0,-2.8]],
    50, ["literal", [0,-3.3]],
    100, ["literal", [0,-3.5]],
  ],
  "text-field": ["step", ["get", "point_count"],
    ["get", "point_count"],
    10, "10+",
    30, "30+",
    50, "50+",
    100, "100+",
    200, "200+",
    300, "300+",
    400, "400+",
  ],
  "text-font": ["Open Sans Bold", "Arial Unicode MS Bold"],
  "text-size": ["step", ["get", "point_count"],
    13,
    10, 15,
    30, 17,
    50, 19,
    100, 22
  ],
};

export const clusterLayerPaint = { "text-color": "#fff" }

export let symbolLayerFilter = ["!", ["has", "point_count"]]

export const symbolLayerLayout = {
	"icon-allow-overlap": true,
	"icon-anchor": "bottom",
	"icon-image": iconImageExpression,
	"icon-offset": [0, 7 * pixelRatio],
	"icon-size": pixelRatio > 1 ? 0.5 : 1,
}

export const symbolLayerPaint = {
	"icon-opacity": ["case", ["boolean", ["feature-state", "highlighted"], false], 1, 0.8],
	"text-color": "#333333",
}
