export default {
	banSearchUrl: process.env.BAN_SEARCH_URL,
	browserlessApiToken: process.env.BROWSERLESS_API_TOKEN,
	defaultLanguage: process.env.DEFAULT_LANGUAGE,
	mapboxAccessToken: process.env.MAPBOX_ACCESS_TOKEN,
	structuresUrl: process.env.STRUCTURES_URL,
	enrichTheMapUrl: process.env.ENRICH_THE_MAP_URL,
	francilinUrl: process.env.FRANCILIN_URL,
}
