import svelte from "rollup-plugin-svelte-hot"
// import svelteConfig from "./svelte.config.js"
const svelteConfig = require("./svelte.config")
import Hmr from "rollup-plugin-hot"
import resolve from "@rollup/plugin-node-resolve"
import commonjs from "@rollup/plugin-commonjs"
import livereload from "rollup-plugin-livereload"
import { terser } from "rollup-plugin-terser"
import dotenv from "rollup-plugin-dotenv"
import { string } from "rollup-plugin-string"
import json from "@rollup/plugin-json"
import alias from "@rollup/plugin-alias"
import { copySync, removeSync } from "fs-extra"
import { spassr } from "spassr"
import getConfig from "@roxi/routify/lib/utils/config"
// import css from "rollup-plugin-css-only"

// because getConfig() use configent which associate .env file content to process.env,
// and not .env.local, we need to configure rollup-plugin-dotenv before
const dotenvConfig = dotenv()
const { distDir } = getConfig() // use Routify's distDir for SSOT
const publicDir = "public"
const buildDir = `${distDir}/build`
const isNollup = !!process.env.NOLLUP
const production = !process.env.ROLLUP_WATCH
process.env.NODE_ENV = production ? "production" : "development"

// clear previous builds
removeSync(distDir)
removeSync(buildDir)

const serve = () => ({
	writeBundle: async () => {
		const options = {
			assetsDir: [publicDir, distDir],
			entrypoint: `${publicDir}/__app.html`,
			script: `${buildDir}/main.js`,
		}
		spassr({ ...options, port: 5000 })
		spassr({
			...options,
			ssr: true,
			port: 5005,
			ssrOptions: { inlineDynamicImports: true, dev: true },
		})
	},
})
const copyToDist = () => ({
	writeBundle() {
		copySync(publicDir, distDir)
	},
})
export default {
	preserveEntrySignatures: false,
	input: [`src/main.js`],
	output: {
		sourcemap: !production,
		format: "esm",
		dir: buildDir,
		// for performance, disabling filename hashing in development
		chunkFileNames: `[name]${(production && "-[hash]") || ""}.js`,
	},
	plugins: [
		dotenvConfig,

		svelte(svelteConfig),

		// TODO extract css into separate file
		// css({ output: "main.css" }),

		// resolve matching modules from current working directory
		resolve({
			browser: true,
			dedupe: (importee) => !!importee.match(/svelte(\/|$)/),
		}),
		commonjs(),
		alias({ entries: [{ find: "~", replacement: __dirname + "/src" }] }),

		json(),

		string({
			include: "src/icons/**/*.svg",
		}),

		production && terser(),
		!production && !isNollup && serve(),
		!production && !isNollup && livereload(distDir), // refresh entire window when code is updated
		!production && isNollup && Hmr({ inMemory: true, public: publicDir }), // refresh only updated code
		{
			// provide node environment on the client
			transform: (code) => ({
				code: code.replace(/process\.env\.NODE_ENV/g, `"${process.env.NODE_ENV}"`),
				map: { mappings: "" },
			}),
		},
		production && copyToDist(),
	],
	watch: {
		clearScreen: false,
		buildDelay: 100,
	},
}
