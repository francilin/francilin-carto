import structure_accessibilite from "./structure_accessibilite.svg"
import structure_accompagnement from "./structure_accompagnement.svg"
import structure_coordonnees from "./structure_coordonnees.svg"
import structure_horaires from "./structure_horaires.svg"
import structure_itineraires from "./structure_itineraires.svg"
import structure_label from "./structure_label.svg"
import structure_services from "./structure_services.svg"
import structure_remarques from "./structure_remarques.svg"

export default {
	structure_accessibilite,
	structure_accompagnement,
	structure_coordonnees,
	structure_horaires,
	structure_itineraires,
	structure_label,
	structure_services,
	structure_remarques,
}
