const production = !process.env.ROLLUP_WATCH

module.exports = {
	purge: {
		content: ["./src/**/*.svelte"],
		enabled: production,
	},
	theme: {
		colors: {
			gray: {
				100: "#f4f5f7",
				200: "#e5e5e5",
				// "300": "#d2d6dc",
				300: "#cbd5e0",
				// "350": "#B9BEC7",
				400: "#9fa6b2",
				500: "#718096",
				600: "#4b5563",
				700: "#374151",
				800: "#252f3f",
				900: "#1e1e1c",
			},
			orange: {
				// 100: "#FDE8D8",
				// 200: "#FBCBA7",
				// 300: "#F9AE76",
				// 400: "#F69146",
				500: "#F47415",
				600: "#CC5D0A",
				// 700: "#9C4707",
				// 800: "#6B3005",
				// 900: "#3A1A03",
			},
			blue: {
				// 100: "#7AB3FF",
				// 200: "#4796FF",
				// 300: "#1479FF",
				// 400: "#0060E0",
				500: "#004aad",
				// 600: "#00347A",
				// 700: "#001E47",
				// 800: "#000914",
				// 900: "#000000",
			},
			pink: {
				100: "#F6E0E5",
				200: "#F0CCD4",
				// 300: "#E4A5B3",
				// 400: "#D97D91",
				500: "#cd5670",
				// 600: "#B93753",
				700: "#922B42",
				// 800: "#6A2030",
				// 900: "#43141E",
			},
			black: "#000000",
			white: "#ffffff",
			mapbox: {
				shadow: "rgba(0, 0, 0, .1)",
			},
			transparent: "transparent",
		},
		extend: {
			screens: {
				print: { raw: "print" },
			},
			boxShadow: {
				mapbox: "0 0 0 2px rgba(0,0,0,.1)",
				pink: "0 0 0 2px #cd5670",
			},
		},
		boxShadow: (theme) => ({
			inner: "inset 0 0 0 2px " + theme("colors.gray.700"),
		}),
	},
	plugins: [
		require("@tailwindcss/forms")({
			strategy: "class",
		}),
		require("tailwindcss-rtl"),
	],
}
