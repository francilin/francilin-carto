let pixelRatio = Math.min(2, Math.floor(window.devicePixelRatio))

export let mapLoader = (map) => {
	return new Promise((res) => {
		map.on("load", async () => {
			res(map)
		})
	})
}

async function loadImage(map, url, name) {
	return new Promise((resolve, reject) => {
		map.loadImage(url, function (error, image) {
			if (error) {
				reject(error)
			}
			resolve({
				url,
				name,
				image,
			})
		})
	})
}

export async function imagesLoader(map) {
	let suffix = pixelRatio < 2 ? "" : "_hd"
	let images = []
	for (let i = 0; i <= 10; i++) {
		images.push(loadImage(map, `/marker-icons/theme_${i}${suffix}.png`, `theme-${i}`))
	}

	images.push(loadImage(map, "/marker-icons/marker-cluster.png", "marker-cluster"))

	return Promise.all(images)
}

export async function handleImageMissing({ id, target: map }) {
	console.log("imageMissing", id)
	let url
	if (id === "marker-cluster") {
		url = "/marker-icons/marker-cluster.png"
	} else if (id.indexOf("theme-") === 0) {
		let suffix = pixelRatio < 2 ? "" : "_hd"
		url = `/marker-icons/marker_${id.substr(6)}${suffix}.png`
	} else {
		console.log("unhandled imagemissing", id)
		return
	}
	await loadImage(map, url, id)
}
