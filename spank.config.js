module.exports = {
	blacklist: ["/map/index", "/map", "/index", "/wizard", "/wizard/index"],
	depth: 1,
	ssrOptions: {
		timeout: 10000,
	},
}
