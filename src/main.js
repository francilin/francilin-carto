import HMR from "@roxi/routify/hmr"
import App from "./App.svelte"
import { initLocales } from "./i18n.js"

initLocales()

const app = HMR(App, { target: document.body }, "routify-app")

export default app
