# Francil'IN Carto

## Install

The application was developed using Node v14. You can install it using [nvm](https://github.com/nvm-sh/nvm) for example.

```bash
# if you are running an earlier version of node.js 14, you are going to need to install Node Version Manager
nvm use 14
```

Install the dependencies...

```bash
cd francilin-carto
npm i
```

create a `.env.local` file with `.env` values you want to overwrite

```bash
# example
MAPBOX_ACCESS_TOKEN=XXX
BROWSERLESS_API_TOKEN=XXX

# if you want to test local/custom data
STRUCTURES_URL=http://127.0.0.1:8080/structures.geojson
STRUCTURES_URL=https://dev--data-francilin.netlify.app/structures.geojson

```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

## Building and running in production mode

Commits to `master` are deployed to production with Netlify to [https://carto-francilin.netlify.app](https://carto-francilin.netlify.app).

Commits to other branches and merge requests are built and previews are attached to commits.

## Details

The App is developped with Svelte and Routify in order to add the routing to our application. Routify comes with a not very well known but practical eco system.

### [Nollup](https://github.com/PepsRyuu/nollup)

Rollup compatible bundler, designed to be used in development. Using the same Rollup plugins and configuration (`rollup.config.js`), it provides a dev server that performs quick builds and rebuilds, and other dev features such as Hot Module Replacement.

### [SpaSSR](https://github.com/roxiness/spassr) Spa server with SSR

It is a small express server with/without SSR. It offers the possibility to define an entry point if the url does not correspond to a file : `__app.html`.

We use it :

- with `npm run serve` without ssr when we want to test locally a built version of the application.
- with `npm run dev:rollup` with ssr. You don't need SSR in your app but the same process is used by spank for Static Site Generation and with this it's easy to control what content will be generated server side. (`http://localhost:5005`) Verify to use npm version 6, if you use v7 you will get a `esbuild` error.

### [Spank](https://github.com/roxiness/spank) Static Site Generator

Used at build time with `npm run build:static` to generate content for SEO.

### Scripts

`nollup` and `routify` are intended to be used with `npm run dev` or `npm run dev:rollup`. Don't execute them separately.

## Updating i18n messages

Source message file is `src/messages/messages.csv`

To make your changes effective, call (in `src/messages` folder):

```
./msg2json.py
```

`src/messages/fr.json` and `src/messages/en.json` will be re-generated.

## Updating services

Services are stored as i18n messages

To add a new service, just add a new message which key's like `service.{serviceId}.label`. Please, keep in mind that first digit of `service id` is the `theme id` of the theme that the service belongs to.

## Updating themes

Themes are also stored as i18n messages

To add a new theme:

- add a new message which key's like `theme.{themeId}.label`
- add a new message which key's like `theme.{themeId}.description`

To make it appear in Theme filter page, add its themeId to `THEME_ORDER` list constant
defined in `src/constants.js`
