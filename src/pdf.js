import { saveAs } from "file-saver"
import config from "~/config.js"
import {
	logoFrancilIn,
	logoBanqueDesTerritoires,
	logoMissionSocieteNumerique,
	logoRepubliqueFrancaise,
} from "./icons/logos.js"

let francilinHostname = new URL(config.francilinUrl).host

// because we use a custom layout for printing, link of structure is prefixed with /printable
export function printableUrlToScreen(url) {
	let urlParsed = new URL(url)
	if (urlParsed.pathname.indexOf("/printable") === 0) {
		return urlParsed.origin + urlParsed.pathname.substring("/printable".length)
	} else {
		return url
	}
}

export async function saveUrlToPdf(url, filename, errorMessage) {
	const apiUrl = config.browserlessApiToken
		? `https://chrome.browserless.io/pdf?token=${config.browserlessApiToken}`
		: `https://chrome.browserless.io/pdf`
	let urlParsed = new URL(url)
	let origin = urlParsed.origin

	let printableUrl = `${origin}/printable${urlParsed.pathname}`

	let response
	try {
		// styles must be inline and images base64 encoded
		response = await fetch(apiUrl, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				url: printableUrl,
				options: {
					displayHeaderFooter: true,
					headerTemplate: `
					<div style="width: 800px; height: 96px; display: flex; justify-content: center; align-items: flex-start;">
						${logoFrancilIn}
					</div>
					`,
					footerTemplate: `
						<div style="width: 800px;">
							<div style="width: 100%; height: 48px; display: flex; justify-content: center; align-items: flex-end;">
								${logoBanqueDesTerritoires}
								${logoRepubliqueFrancaise}
								${logoMissionSocieteNumerique}
							</div>
							<div style="width: 100%; text-align: center; margin-top: 4px">
								<a href="${config.francilinUrl}" style="color: #000; font-size: 6pt; font-style: italic;" target="_blank">${francilinHostname}</a>
							</div>
						</div>
					`,
					preferCSSPageSize: false,
					margin: { top: "30mm", right: "8mm", bottom: "30mm", left: "8mm" },
					printBackground: false,
					format: "A4",
				},
				gotoOptions: {
					waitUntil: "networkidle0",
				},
			}),
		})
	} catch (error) {
		alert(errorMessage)
		return
	}
	const blob = await response.blob()
	saveAs(blob, filename)
}
