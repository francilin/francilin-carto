import { get, writable, derived, readable } from "svelte/store"
import { locale, locales } from "svelte-i18n"
import { writable as persistentWritable } from "svelte-persistent-store/local.js"
// import { writable as persistentWritable } from "svelte-persistent-store/dist/local"
import * as api from "~/api.js"
import {
	AIDE,
	THEME,
	URGENCE,
	AIDE_LABEL_PASS_NUMERIQUE,
	AIDE_LABEL_CONSEILLER_NUMERIQUE,
	AIDE_LABEL_AIDANTS_CONNECT,
} from "./constants.js"

// Settings

const LOCAL_STORAGE_KEY = "francilin-carto"

function createLocalStorageSettings() {
	const store = persistentWritable(LOCAL_STORAGE_KEY, {})

	return {
		...store,
		merge: (newValues) => {
			store.update((currentValues) => ({ ...currentValues, ...newValues }))
		},
	}
}

export const localStorageSettings = createLocalStorageSettings()

localStorageSettings.subscribe((value) => {
	if (value.locale && get(locale) !== value.locale) {
		locale.set(value.locale)
	}
})

locale.subscribe((value) => {
	localStorageSettings.merge({ locale: value })
})

// I18n

const localeItems = [
	{ value: "fr", label: "Français" },
	{ value: "en", label: "English" },
	{ value: "sp", label: "Español" },
	{ value: "ar", label: "عرب" },
	{ value: "zh", label: "中文" },
]

export const availableLocaleItems = derived(locales, ($locales) =>
	localeItems.filter(({ value }) => $locales.includes(value)),
)

export const availableLocaleItemsSelect = derived(locales, ($locales) => {
	let obj = {}
	localeItems
		.filter(({ value }) => $locales.includes(value))
		.forEach((loc) => {
			obj[loc.value] = loc.label
		})
	return obj
})

export const localeItem = derived([locale, availableLocaleItems], ([$locale, $availableLocaleItems]) =>
	$availableLocaleItems.find(({ value }) => value === $locale),
)

// Labels

export const labelsWithIcon = readable([
	AIDE_LABEL_PASS_NUMERIQUE,
	AIDE_LABEL_AIDANTS_CONNECT,
	AIDE_LABEL_CONSEILLER_NUMERIQUE,
])

// Filters

function createFilters() {
	const defaultFilters = {
		[AIDE]: null,
		[THEME]: null,
		[URGENCE]: null,
	}
	const store = writable(defaultFilters)
	const { set, subscribe, update } = store
	return {
		anyDefined: (value) => Object.values(value).filter((value) => value !== null).length > 0,
		reset: () => {
			set(defaultFilters)
		},
		setFilter: (name, value) => {
			update((filters) => ({ ...filters, [name]: value }))
		},
		subscribe,
	}
}

export const filters = createFilters()
// filters.subscribe((f) => {
// 	let labels = f.aide ? f.aide.labels : []
// 	let aide = f.aide ? f.aide.accompagnement : null
// 	console.log(`filtres theme:${f.theme}, urgence:${f.urgence}, aide:${aide}, labels:${labels.join(",")}`)
// })
// Map location

function createMapLocation() {
	const ADDRESS = "ADDRESS"
	const COORDINATES = "COORDINATES"
	const GEOLOCATE = "GEOLOCATE"
	const store = writable(null)
	const { set, subscribe } = store
	const methods = {
		isAddressMode: (value) => {
			return value !== null && value.mode === ADDRESS
		},
		isCoordinatesMode: (value) => {
			return value !== null && value.mode === COORDINATES
		},
		isGeolocateMode: (value) => {
			return value !== null && value.mode === GEOLOCATE
		},
		reset: () => {
			set(null)
		},
		setAddress: (value) => {
			set({ mode: ADDRESS, value })
		},
		setCoordinates: (value) => {
			set({ mode: COORDINATES, value })
		},
		setGeolocateMode: () => {
			set({ mode: GEOLOCATE })
		},
		subscribe,
	}
	return methods
}

export const mapLocation = createMapLocation()

// Structures are a GeoJSON FeatureCollection.
export const structures = writable(null)

// Data fetching
export async function fetchInitialMetaData() {
	if (get(structures)) {
		return
	}
	const structuresData = await api.fetchStructures()
	structures.set(structuresData)
}

export const preRender = readable(document.visibilityState === "prerender")
