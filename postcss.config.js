const production = !process.env.ROLLUP_WATCH

module.exports = {
	plugins: [
		require("postcss-import"),
		require("tailwindcss/nesting"),
		require("tailwindcss"),
		...(production ? [require("autoprefixer"), require("cssnano")({ preset: "default" })] : []),
	],
}
