import SimpleOpeningHours from "simple-opening-hours"

export function isOpenNow(feature) {
	const openingHoursProperty = feature.properties.horaires

	if (!openingHoursProperty) {
		return null
	}
	const openingHours = new SimpleOpeningHours(openingHoursProperty)
	return openingHours.isOpen()
}

export function formatSiret(value) {
	return [value.slice(0, 3), value.slice(3, 6), value.slice(6, 9), value.slice(10)].join(" ")
}

export function normalizeProperties(properties) {
	return {
		francilin_id: properties.francilin_id || null,
		SIRET: properties.SIRET || null,
		lieu_nom: properties.lieu_nom || null,
		contact_nom: properties.contact_nom || null,
		adresse: properties.adresse || null,
		code_postal: properties.code_postal || null,
		commune: properties.commune || null,
		e_mail: properties.e_mail || null,
		telephone: properties.telephone || null,
		thematique_principale: properties.thematique_principale || "0",
		site_web: properties.site_web || null,
		horaires: properties.horaires || null,
		remarques: properties.remarques || null,
		aide: properties.aide || [],
		accessibilite: properties.accessibilite || null,
		services: properties.services || [],
		label: properties.label || [],
		mise_a_jour: properties.mise_a_jour || [],
	}
}

export function findFeatureById(structures, featureId) {
	return structures.features.find((feature) => featureId === feature.id)
}

export function hasFeatures(structures) {
	return structures && structures.features && structures.features.length > 0
}

// Theme - service ids relationship
//
// Theme ids are 1 or 2 digits string, e.g.  "3", "6", "10"...
// Service ids are 3 or 4 digits string, e.g. "136", "203", "1024"...
// First service id digit is the theme id
export function belongToTheme(serviceId, themeId) {
	let serviceTheme = serviceId.length === 3 ? serviceId[0] : serviceId.substr(0, 2)
	return serviceTheme === themeId
}

export function themeIdFromServiceId(serviceId) {
	if (serviceId.length === 3) {
		return serviceId[0]
	} else if (serviceId.length === 4) {
		return serviceId.substr(0, 2)
	} else {
		throw new Error(`Service id must be 3/4 digits : ${serviceId}`)
	}
}
