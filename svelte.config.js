const sveltePreprocess = require("svelte-preprocess")
const isNollup = !!process.env.NOLLUP

module.exports = {
	emitCss: false,
	hot: isNollup,
	preprocess: sveltePreprocess({
		postcss: require("./postcss.config.js"),
	}),
}
