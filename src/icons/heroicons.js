// From https://heroicons.dev/

export const gridAdd = {
	d: "M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const chevronDown = {
	d: "M19 9l-7 7-7-7",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const externalLink = {
	d: "M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const link = {
	d: "M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const trash = {
	d: "m 10.358414,3.8237151 a 1,1 0 0 0 -0.8939996,0.553 l -0.724,1.447 h -3.382 a 1,1 0 0 0 0,2 v 10 a 2,2 0 0 0 2,2 h 8 a 2,2 0 0 0 2,-2 V 7.8237151 a 1,1 0 1 0 0,-2 h -3.382 l -0.724,-1.447 a 1,1 0 0 0 -0.894,-0.553 z m -2,6 a 1,1 0 0 1 2,0 v 6 a 1,1 0 1 1 -2,0 z m 5,-1 a 1,1 0 0 0 -1,1 v 6 a 1,1 0 1 0 2,0 V 9.8237151 a 1,1 0 0 0 -1,-1 z",
	viewBox: "0 0 24 24",
	variant: "solid",
}

export const close = {
	d: "M6 18L18 6M6 6l12 12",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const translate = {
	d: "M3 5h12M9 3v2m1.048 9.5A18.022 18.022 0 016.412 9m6.088 9h7M11 21l5-10 5 10M12.751 5C11.783 10.77 8.07 15.61 3 18.129",
	viewBox: "0 0 24 24",
	variant: "outline",
}
