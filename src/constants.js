export const THEME = "theme"
// Force display order, cf https://gitlab.com/francilin/francilin-carto/-/issues/18
export const THEME_ORDER = ["1", "2", "3", "5", "4", "8", "7", "6", "9", "10"]

export const AIDE = "aide"
// Use numbers to match values used in the API.
export const AIDE_SEUL = "1"
export const AIDE_AIDE = "2"
export const AIDE_ACCOMPAGNE = "3"
export const AIDE_ATELIER = "4"

export const AIDE_LABEL_PASS_NUMERIQUE = "1"
export const AIDE_LABEL_AIDANTS_CONNECT = "6"
export const AIDE_LABEL_CONSEILLER_NUMERIQUE = "7"

export const URGENCE = "urgence"
export const URGENCE_URGENT = true
export const URGENCE_NON_URGENT = false

export const LIEU = "lieu"
