import { get } from "svelte/store"
import {
	AIDE,
	AIDE_SEUL,
	AIDE_AIDE,
	AIDE_ATELIER,
	AIDE_ACCOMPAGNE,
	AIDE_LABEL_AIDANTS_CONNECT,
	AIDE_LABEL_PASS_NUMERIQUE,
	THEME,
	THEME_ORDER,
	URGENCE,
	URGENCE_URGENT,
	URGENCE_NON_URGENT,
	LIEU,
} from "./constants.js"
import { mapLocation, filters } from "~/stores.js"

export const steps = [
	{
		label: "filter.theme.label",
		name: THEME,
		route: "theme-filter",
		title: "filter.theme.title",
		values: THEME_ORDER.map((code) => ({
			name: code,
			label: `theme.${code}.label`,
			description: `theme.${code}.description`,
		})),
	},
	{
		label: "filter.urgence.label",
		name: URGENCE,
		route: "urgence-filter",
		title: "filter.urgence.title",
		values: [
			{
				name: URGENCE_URGENT,
				label: "filter.urgence.value.1.label",
				description: "filter.urgence.value.1.description",
			},
			{
				name: URGENCE_NON_URGENT,
				label: "filter.urgence.value.2.label",
				description: "filter.urgence.value.2.description",
			},
		],
	},
	{
		label: "filter.aide.label",
		name: AIDE,
		route: "aide-filter",
		title: "filter.aide.title",
		values: [
			{
				name: AIDE_SEUL,
				label: "filter.aide.value.1.label",
				description: "filter.aide.value.1.description",
				labels: [],
			},
			{
				name: AIDE_AIDE,
				label: "filter.aide.value.2.label",
				description: "filter.aide.value.2.description",
				labels: [AIDE_LABEL_PASS_NUMERIQUE],
			},
			{
				name: AIDE_ATELIER,
				label: "filter.aide.value.4.label",
				description: "filter.aide.value.4.description",
				labels: [AIDE_LABEL_PASS_NUMERIQUE],
			},
			{
				name: AIDE_ACCOMPAGNE,
				label: "filter.aide.value.3.label",
				description: "filter.aide.value.3.description",
				labels: [AIDE_LABEL_PASS_NUMERIQUE, AIDE_LABEL_AIDANTS_CONNECT],
			},
		],
	},
	{
		label: "filter.lieu.label",
		name: LIEU,
		route: "location-picker",
		title: "filter.lieu.title",
	},
].map((step, index) => ({ ...step, index }))

export function findStepByName(name) {
	return steps.find((step) => step.name === name)
}

export function findStepByRoute(route) {
	return steps.find((step) => step.route === route)
}

export function getStepValueLabel(name) {
	const step = findStepByName(name)
	if (name === LIEU) {
		return step.label
	}
	const filterValue = get(filters)[name]
	if (name === THEME) {
		return `theme.${filterValue}.label`
	}
	if (name === AIDE) {
		return step.values.find((value) => value.name === filterValue.accompagnement).label
	}
	return step.values.find((value) => value.name === filterValue).label
}

export function getNextStep(name) {
	const index = steps.findIndex((step) => step.name === name)
	const newIndex = index + 1
	return newIndex <= steps.length ? steps[newIndex] : null
}

export function getPreviousStep(name) {
	const index = steps.findIndex((step) => step.name === name)
	const newIndex = index - 1
	return newIndex >= 0 ? steps[newIndex] : null
}

export function resetStepValue(name) {
	if (name === LIEU) {
		mapLocation.reset()
	}
	return filters.setFilter(name, null)
}

export function setStepValue(name, value) {
	if (name === LIEU) {
		throw new Error("Ambiguous: should geolocate mode be set, or an address?")
	}
	return filters.setFilter(name, value)
}
