import aide_1 from "./aide_1.svg"
import aide_2 from "./aide_2.svg"
import aide_3 from "./aide_3.svg"
import aide_4 from "./aide_4.svg"
import aide_default from "./aide_default.svg"

export default {
	aide_1,
	aide_2,
	aide_3,
	aide_4,
	aide_default,
}
