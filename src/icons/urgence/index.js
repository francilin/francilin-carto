import urgence_false from "./urgence_false.svg"
import urgence_true from "./urgence_true.svg"
import urgence_default from "./urgence_default.svg"

export default {
	urgence_true,
	urgence_false,
	urgence_default,
}
